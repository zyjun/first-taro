import React, {Component} from 'react';
import {View} from "@tarojs/components";
import PropTypes from "prop-types";

class Index extends Component {
  static propTypes = {
    text: PropTypes.string.isRequired,
    type: PropTypes.oneOf(['success', 'warning'])
  }
  static defaultProps = {
    type: 'success'
  }

  constructor(props) {
    super(props);
  }

  render() {
    let {text, type} = this.props
    return (
      <View style={{...styles.tagView, ...styles[type]}}>
        {text}
      </View>
    );
  }
}

const styles = {
  tagView: {
    fontSize: '10px',
    height: '18px',
    lineHeight: '18px',
    padding: '0px 6px',
    borderRadius: '18px'
  },
  success: {
    color: '#19be6b',
    backgroundColor: '#dbf1e1',
    border: '1px solid #71d5a1'
  },
  warning: {
    color: '#f90',
    backgroundColor: '#fdf6ec',
    border: '1px solid #fcbd71'
  }
}

export default Index;
