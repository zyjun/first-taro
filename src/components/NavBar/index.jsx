import React, {Component} from 'react';
import {View} from "@tarojs/components";
import Taro from "@tarojs/taro"
import PropTypes from "prop-types";


class Index extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    backType: PropTypes.oneOf(['tab', 'page']),
    backPage: PropTypes.string,
  }
  static defaultProps = {
    backType: 'page',
    backPage: 'pages/home/index',
  }

  constructor(props) {
    super(props);
  }

  handleClick = (e) => {
    console.log(e)
  }
  handleBack = () => {
    let {backType, backPage} = this.props
    if (backType === 'page') Taro.navigateBack({})
    else Taro.switchTab({url: backPage})
  }

  render() {
    return (
      <View className='hor-layout-center' style={styles.navBar}>
        <View style={styles.backView} onClick={this.handleBack}>
          <i className="iconfont" style={styles.backView.backIcon}>&#xe8b5;</i>
        </View>
        <View style={styles.title}>{this.props.title}</View>
        <View style={styles.backView}></View>
      </View>
    );
  }
}

const styles = {
  navBar: {
    position: 'fixed',
    zIndex: 1000,
    width: '100%',
    height: '44px',
    background: '#42B4AB'
  },
  backView: {
    width: '50px',
    height: '44px',
    lineHeight: '44px',
    textAlign: 'center',
    backIcon: {
      fontSize: '22px',
      color: "#fff"
    },
  },
  title: {
    flex: 1,
    color: '#fff',
    fontWeight: 'bold',
    textAlign: 'center',
  }
}
export default Index;
