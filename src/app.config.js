export default {
  pages: [
    'pages/index/index',
    'pages/home/index',
    'pages/notice/index',
    'pages/mine/index',
    'pages/home/enter/enter',
    'pages/home/enter/editInfo/editInfo',
    'pages/home/enter/selectClass/selectClass',
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black',
    navigationStyle:'default'
  },
  tabBar: {
    color: "#92959B",
    selectedColor: "#55BBB3",
    borderStyle: "black",
    backgroundColor: "#F7F7FA",
    list: [
      {
        pagePath: "pages/home/index",
        iconPath: "image/tabbar/ic_home.png",
        selectedIconPath: "image/tabbar/ic_home_sel.png",
        text: "首页"
      },
      {
        pagePath: "pages/notice/index",
        iconPath: "image/tabbar/ic_notice.png",
        selectedIconPath: "image/tabbar/ic_notice_sel.png",
        text: "提示"
      },
      {
        pagePath: "pages/mine/index",
        iconPath: "image/tabbar/ic_mine.png",
        selectedIconPath: "image/tabbar/ic_mine_sel.png",
        text: "我的"
      }
    ]
  }
}
