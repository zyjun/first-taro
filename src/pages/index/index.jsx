import {Component} from "react";
import {Image, Input, View} from "@tarojs/components";
import './index.scss'
import Taro from '@tarojs/taro'

let ic_login_top_bg = require("@/image/ic_login_top_bg.png"),
  ic_login_account = require("@/image/ic_login_account.png"),
  ic_login_yzm = require("@/image/ic_login_yzm.png"),
  ic_login_lock = require("@/image/ic_login_lock.png")

export default class Index extends Component {
  constructor() {
    super(...arguments);
    this.state = {
      countDownTimer: '',//倒计时
      getYzmText: '获取验证码',
      loginType: 0,//0:验证码登录，1：手机号登录
      loginParams: {
        passwd: '',
        phone: '',
        code: '',
      }
    }
  }

  componentDidMount() {

  }

  loginTitle() {
    return this.state.loginType === 0 ? "验证码登录" : "手机号登录"
  }

  loginText() {
    return this.state.loginType === 0 ? "手机号登录" : "验证码登录"
  }

  canLoginUse() {
    let yzmUse = this.state.loginType === 0 && this.state.loginParams.code,
      phoneUse = this.state.loginType === 1 && this.state.loginParams.passwd
    if (yzmUse || phoneUse) return true
    else return false
  }

  loginClick = async () => {
    const {data} = await Taro.request({
      url: 'https://ldjk.ranyi.shop/api/v1/login/pwd/org',
      data: this.state.loginParams,
      method: 'POST'
    })
    console.log(data.data)
  }

  inputPhone = (e) => {
    let {value} = e.detail, loginParams = this.state.loginParams
    this.setState({loginParams: {...loginParams, phone: value}})
  }
  inputPassword = (e) => {
    let {value} = e.detail, loginParams = this.state.loginParams
    this.setState({loginParams: {...loginParams, passwd: value}})
  }

  render() {
    let {getYzmText} = this.state
    return (
      <View className='page'>
        <Image className="top-bg" src={ic_login_top_bg}></Image>
        <View className="login-box ver-layout">
          <View className="login-title">{this.loginTitle}</View>
          {/*手机号输入*/}
          <View className="login-item hor-layout-center" style="margin-top: 89rpx">
            <Image className="login-icon" src={ic_login_account}></Image>
            <Input onInput={this.inputPhone} type="number" maxlength="11" className="input-phone"
                   placeholder-class="input-placeholder" placeholder="请输入手机号"/>
          </View>
          {/*验证码输入*/}
          <View className="login-item hor-layout-center" v-if="loginType===0"
                style="margin-top: 50rpx;padding-bottom: 18rpx">
            <Image className="login-icon" src={ic_login_yzm}></Image>
            <Input v-model="loginParams.code" type="number" maxlength="6" className="input-phone"
                   placeholder-class="input-placeholder" placeholder="请输入验证码"/>
            <View className="get-yzm-btn" className="{ 'disabled': !canGetYzmUse }">{getYzmText}</View>
          </View>
          {/*密码输入*/}
          <View className="login-item hor-layout-center" v-if="loginType===1" style="margin-top: 67rpx">
            <Image className="login-icon" src={ic_login_lock}></Image>
            <Input onInput={this.inputPassword} type="number" maxlength="6" className="input-phone"
                   placeholder-class="input-placeholder" placeholder="请输入密码"/>
          </View>
          <View className="change-login-type hor-layout-side">
            <View>{this.loginText}</View>
            <View style="color: #999999">忘记密码</View>
          </View>
          {/*登录*/}
          <View className={['login-btn', this.canLoginUse ? '' : 'disabled'].join(" ")} onClick={this.loginClick}>
            登录
          </View>
          <View className="login-hint">点击登录代表您已同意
            <span className="hint-link"> 用户协议 </span>和
            <span className="hint-link"> 隐私政策 </span>
          </View>
        </View>
      </View>
    )
  }
}
