import React, {Component} from 'react';
import {Image, View} from "@tarojs/components";
import NavBar from "@/components/NavBar"
import {editInfo, selectClass} from "@/router.js"
import Taro from "@tarojs/taro"

class Enter extends Component {

  constructor() {
    super(...arguments);
    this.state = {
      oneToOne: require('@/image/ic_enter_one.png'),
      oneToMore: require('@/image/ic_enter_all.png'),
    }
  }

  onLoad() {
    console.log("onLoad===============")
  }

  naviToPage = (page) => {
    console.log(page)
    Taro.navigateTo({url: page})
  }

  render() {
    let {oneToOne, oneToMore} = this.state
    return (
      <View className='page'>
        <NavBar title='学员报名' backType='tab'></NavBar>
        <View className='app-container'>
          <View>选择模式</View>
          <View className='hor-layout-center' style={styles.modelView} onClick={() => this.naviToPage(editInfo)}>
            <Image style={styles.modelIcon} src={oneToOne}/>
            <View style={styles.modelText}>一对一</View>
          </View>
          <View className="hor-layout-center" style={styles.modelView} onClick={() => this.naviToPage(selectClass)}>
            <Image style={styles.modelIcon} src={oneToMore}/>
            <View style={styles.modelText}>集体课</View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = {
  modelView: {
    background: 'white',
    border: '1px solid #E5E5E5',
    boxShadow: '1px 1px 1px #e3e3e3',
    borderRadius: '13px',
    padding: '10px 6px',
    marginTop: '15px'
  },
  modelIcon: {
    width: '42px',
    height: '42px',
  },
  modelText: {
    fontSize: '17px',
    fontWeight: 'bold',
    color: '#333333',
    marginLeft: '23px',
  }
}
export default Enter;
