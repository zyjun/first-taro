import React, {Component} from 'react';
import {Image, View} from "@tarojs/components";
import NavBar from "@/components/NavBar"
import "./editInfo.scss"
import {AtIcon} from "taro-ui";

class EditInfo extends Component {
  render() {
    return (
      <View className='page'>
        <NavBar title='学员资料填写'></NavBar>
        <View className='page-container app-container'>
          {/*上传照片*/}
          <View className='upload-layout hor-layout-center'>
            <View className='photo-view ver-layout-center-all'>
              <AtIcon value='camera' size='40px' color='rgb(153, 153, 153)'></AtIcon>
            </View>
            <View className='upload-text'>上传照片</View>
          </View>

        </View>
      </View>
    );
  }
}

export default EditInfo;
