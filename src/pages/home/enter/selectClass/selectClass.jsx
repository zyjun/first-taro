import React, {Component} from 'react';
import {View} from "@tarojs/components";
import NavBar from "@/components/NavBar"
import styles from "./selectClass.module.scss"
import {AtIcon} from "taro-ui";

class EditInfo extends Component {
  constructor() {
    super(...arguments);
    this.state = {
      classList: [
        {
          "class_id": 48,
          "class_title": "\u5982\u56fe\u6d82\u4e8e\u592a\u7389\u56ed",
          "class_major": "",
          "course_id": 42,
          "course_sku_id": 5,
          "course_title": "\u5409\u4ed6\u7b80\u6613\u6559\u7a0b",
          "teacher_name": "\u738b\u8001\u5e08",
          "student_total": 0,
          "start_time": "2021.11.18",
          "created_at": "2021.11.02"
        },
        {
          "class_id": 29,
          "class_title": "ccccc",
          "class_major": "",
          "course_id": "",
          "course_sku_id": 0,
          "course_title": "",
          "teacher_name": "",
          "student_total": 0,
          "start_time": "2021.10.23",
          "created_at": "2021.10.23"
        },
        {
          "class_id": 70,
          "class_title": "\u6d4b\u8bd5213413",
          "class_major": "\u94a2\u7434\u4e13\u4e1a",
          "course_id": 36,
          "course_sku_id": 1,
          "course_title": "\u7b80\u6613\u94a2\u7434\u6559\u7a0b",
          "teacher_name": "\u738b\u8001\u5e08",
          "student_total": 0,
          "start_time": "2021.11.27",
          "created_at": "2021.11.04"
        },
        {
          "class_id": 66,
          "class_title": "\u51b7\u9177\u8ba1\u5212",
          "class_major": "\u5409\u4ed6\u4e13\u4e1a",
          "course_id": 43,
          "course_sku_id": 6,
          "course_title": "\u5409\u4ed6\u7b80\u6613\u6559\u7a0b",
          "teacher_name": "Jtipsy",
          "student_total": 1,
          "start_time": "2021.11.21",
          "created_at": "2021.11.03"
        },
        {
          "class_id": 65,
          "class_title": "\u4eac\u54c8\u5b89\u5168",
          "class_major": "\u94a2\u7434\u4e13\u4e1a",
          "course_id": 42,
          "course_sku_id": 4,
          "course_title": "\u5409\u4ed6\u7b80\u6613\u6559\u7a0b",
          "teacher_name": "\u738b\u8001\u5e08",
          "student_total": 0,
          "start_time": "2021.11.03",
          "created_at": "2021.11.03"
        },
        {
          "class_id": 57,
          "class_title": "\u4e0a\u8bfe\u4e0a\u8bfe\u4e86",
          "class_major": "\u5409\u4ed6\u4e13\u4e1a",
          "course_id": 43,
          "course_sku_id": 6,
          "course_title": "\u5409\u4ed6\u7b80\u6613\u6559\u7a0b",
          "teacher_name": "\u738b\u8001\u5e08",
          "student_total": 1,
          "start_time": "2021.11.05",
          "created_at": "2021.11.03"
        },
        {
          "class_id": 49,
          "class_title": "sku2\u6d4b\u8bd5",
          "class_major": "\u53e4\u7b5d\u4e13\u4e1a",
          "course_id": 44,
          "course_sku_id": 7,
          "course_title": "\u53e4\u7b5d\u5f39\u594f",
          "teacher_name": "\u738b\u8001\u5e08",
          "student_total": 3,
          "start_time": "2021.11.20",
          "created_at": "2021.11.02"
        },
        {
          "class_id": 47,
          "class_title": "sku3\u6d4b\u8bd5",
          "class_major": "\u94a2\u7434\u4e13\u4e1a",
          "course_id": 36,
          "course_sku_id": 1,
          "course_title": "\u7b80\u6613\u94a2\u7434\u6559\u7a0b",
          "teacher_name": "\u738b\u8001\u5e08",
          "student_total": 0,
          "start_time": "2021.11.19",
          "created_at": "2021.11.02"
        },
        {
          "class_id": 46,
          "class_title": "sku2\u6d4b\u8bd5",
          "class_major": "\u94a2\u7434\u4e13\u4e1a",
          "course_id": 42,
          "course_sku_id": 5,
          "course_title": "\u5409\u4ed6\u7b80\u6613\u6559\u7a0b",
          "teacher_name": "\u738b\u8001\u5e08",
          "student_total": 0,
          "start_time": "2021.11.19",
          "created_at": "2021.11.02"
        },
        {
          "class_id": 38,
          "class_title": "sku\u6d4b\u8bd5\u73ed\u7ea7",
          "class_major": "\u53e4\u7b5d\u4e13\u4e1a",
          "course_id": 44,
          "course_sku_id": 7,
          "course_title": "\u53e4\u7b5d\u5f39\u594f",
          "teacher_name": "\u738b\u8001\u5e08",
          "student_total": 3,
          "start_time": "2021.11.05",
          "created_at": "2021.11.02"
        }
      ]
    }
  }

  async componentDidMount() {
    const res = await Taro.request({
      url: 'test.php', //仅为示例，并非真实的接口地址
      data: {
        x: '',
        y: ''
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
    })
  }

  render() {
    let {classList} = this.state
    return (
      <View className={['page', styles.page].join(' ')}>
        <NavBar title='选择班级'></NavBar>
        <View className='page-container'>
          {/*上面选择栏*/}
          <View className={['hor-layout-side-center', styles.topLayout].join(" ")}>
            <View className={styles.topTitle}>古筝专业</View>
            <View className={styles.selectTypeView}>
              分类
              <AtIcon value='chevron-down' size='30' color='#000'></AtIcon>
            </View>
          </View>
          {/* 班级列表 */}
          <View className={styles.classListView}>
            {classList.map((k, i) => {
              return <View className={['ver-layout', styles.classItem].join(" ")}>
                <View className='hor-layout-side-center'>
                  <View className={styles.classTitle}>{k.class_title}</View>
                  <View className={styles.classNum}>{k.student_total}人</View>
                </View>
                <View className='hor-layout-side-center' style="margin-top: 18px">
                  <View className={styles.classTime}>开班时间{k.start_time}</View>
                  <View className={styles.classType}>{k.class_major}</View>
                </View>
              </View>
            })}
          </View>
        </View>
      </View>
    );
  }
}

export default EditInfo;
