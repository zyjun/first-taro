import {Component} from 'react'
import {View, Image} from '@tarojs/components'
import {AtGrid, AtIcon, AtTag} from 'taro-ui'
import Taro from '@tarojs/taro'
import TagView from "@/components/TagView"
import './index.scss'
import './index.config'
import router from "@/router.js"

let ic_home_enter_student = require("@/image/home/ic_home_enter_student.png")
  , ic_home_teacher_manage = require("@/image/home/ic_home_teacher_manage.png")
  , ic_home_student_manage = require("@/image/home/ic_home_student_manage.png")
  , ic_home_ask_break = require("@/image/home/ic_home_ask_break.png")
  , ic_home_course_manage = require("@/image/home/ic_home_course_manage.png")
  , ic_home_student_info = require("@/image/home/ic_home_student_info.png")
  , ic_home_course_live = require("@/image/home/ic_home_course_live.png")
  , ic_home_my_customer = require("@/image/home/ic_home_my_customer.png")
  , ic_home_suggest_recomd = require("@/image/home/ic_home_suggest_recomd.png")
  , ic_home_check_info = require("@/image/home/ic_home_work_info.png")
  , ic_home_todo = require("@/image/home/ic_home_todo.png")
  , ic_home_more = require("@/image/home/ic_home_more.png")

export default class Index extends Component {
  constructor() {
    super(...arguments);
    this.state = {
      gridList: [
        {text: '学员报名', icon: ic_home_enter_student, action: router.enterSd},
        {text: '教师管理', icon: ic_home_teacher_manage, action: router.teacher},
        {text: '学生管理', icon: ic_home_student_manage, disabled: true},
        {text: '请假审批', icon: ic_home_ask_break, action: router.askBreak},
        {text: '课程管理', icon: ic_home_course_manage, disabled: true},
        {text: '学籍管理', icon: ic_home_student_info, action: router.recordStudy},
        {text: '直播监课', icon: ic_home_course_live, action: router.courseLiveList},
        {text: '我的客户', icon: ic_home_my_customer, disabled: true},
        {text: '投诉建议', icon: ic_home_suggest_recomd, action: router.suggestBack},
        {text: '考勤中心', icon: ic_home_check_info, action: router.teacher},
        {text: '备忘录', icon: ic_home_todo, action: router.todoList},
        {text: '员工列表', icon: ic_home_student_manage, disabled: true},
        {text: '录播课程', icon: ic_home_course_manage, action: router.teacher},
        {text: '商品管理', icon: ic_home_suggest_recomd, action: router.goodsList},
        {text: '更多', icon: ic_home_more, disabled: true},
      ],
      todoList: [
        {hint: '续费', text: '有一位学员需通知续费'},
        {hint: '分班', text: '有一个同学报名课程未分班'},
        {hint: '续费', text: '有一位学员需通知续费'},
        {hint: '分班', text: '有一个同学报名课程未分班'},
        {hint: '分班', text: '有一个同学报名课程未分班'},
      ],
    }
  }

  componentWillMount() {
  }

  componentDidMount() {
  }

  componentWillUnmount() {
  }

  componentDidShow() {
  }

  componentDidHide() {
  }

  navToPage = (item) => {
    console.log(item)
    Taro.navigateTo({url: item.action})
  }

  render() {
    return (
      <View className='page'>
        {/*功能按钮*/}
        <View className='flex-grid-layout'>
          {
            this.state.gridList.map((k, i) => {
              return <View className='home-grid-item ver-layout-center' key={i} onClick={() => this.navToPage(k)}>
                <Image src={k.icon} className={['grid-icon', k.disabled ? "disabled" : null].join(' ')}/>
                <View className='grid-text'>{k.text}</View>
              </View>
            })
          }
        </View>
        {/*学员统计*/}
        <View className='divider-line'></View>
        <View className='ver-layout student-manage-layout'>
          <View className='student-manage-title hor-layout-side-center'>
            <View className='title'>学员统计</View>
            <View className='detail'>详细数据</View>
          </View>
          <View className='hor-layout'>
            <View className='manage-item ver-layout-center'>
              <View className="num">1</View>
              <View className="title">学员总计</View>
            </View>
            <View className='manage-item ver-layout-center'>
              <View className="num">1</View>
              <View className="title">学员总计</View>
            </View>
            <View className='manage-item no-border ver-layout-center'>
              <View className="num">1</View>
              <View className="title">学员总计</View>
            </View>
          </View>
        </View>
        {/* 待办事项 */}
        <View className='divider-line'></View>
        <View className='todo-layout'>
          <View className="hor-layout-side todo-title">
            <View>待办事项</View>
            <View className='hor-layout-center-all'>
              <View>更多</View>
              <AtIcon value='chevron-right' size='30' color='#333333'></AtIcon>
            </View>
          </View>
          {
            this.state.todoList.map((k, i) => {
              return <View className='todo-item hor-layout-center' key={i}>
                <TagView text={k.hint} type={k.hint === '续费' ? 'success' : 'warning'}></TagView>
                <View className='todo-text'>{k.text}</View>
              </View>
            })
          }
        </View>
      </View>
    )
  }
}
